import React from 'react'
import Footer from '../companenthome/Footer'
import Homefood from '../Compenetfood/Homefood'
import Navbar from '../companenthome/Navbar'

const Food = () => {
  return (
    <div>
     <Navbar/>
      <Homefood/>
      <Footer/>
    </div>
  )
}

export default Food

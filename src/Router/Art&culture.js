import React from 'react'
import Navbar from '../companenthome/Navbar'
import Footer from '../companenthome/Footer'
import Arthome from '../compenetartcult/Homeimgart'
import Art from '../compenetartcult/Art'
import Cloths from '../compenetartcult/Cloths'
import Music from '../compenetartcult/Music'

const Aart = () => {
  return (
    <div>
  <Navbar/>
  <Arthome/>
  <Art/>
  <Cloths/>
  <Music/>
  <Footer/>
    </div>
  )
}

export default Aart

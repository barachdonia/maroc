import React from 'react'
import Navbar from '../companenthome/Navbar'
import Traditionhome from '../CompenetTradition/Hometradition'
import Weeding from '../CompenetTradition/Weeding'
import Sook from '../CompenetTradition/Sook'
import Tbourida from '../CompenetTradition/Tbourida'
import Footer from '../companenthome/Footer'
import Fistival from '../CompenetTradition/Fistival'

const Tradition = () => {
  return (
    <div>
      <Navbar/>
      <Traditionhome/>
      <Weeding/>
      <Sook/>
      <Tbourida/>
      <Fistival/>
      <Footer/>
    </div>
  )
}

export default Tradition

import React from 'react'
import Navbar from '../companenthome/Navbar'
import Footer from '../companenthome/Footer'
import Homeimg from '../CompanentNature/Homeimg'
import Atlas from '../CompanentNature/Atlas'
import Ouzoud from '../CompanentNature/Ouzoud'
import Akchor from '../CompanentNature/Akchor'
import Figuig from '../CompanentNature/Figuig'
import Imlil from '../CompanentNature/Imlil'

const Nature = () => {
  return (
    <div>
      <Navbar/>
      <Homeimg/>
      <Atlas/>
      <Ouzoud/>
      <Akchor/>
      <Figuig/>
      <Imlil/>
      <Footer/>
    </div>
  )
}

export default Nature

import React, { useState } from 'react';
import Swal from 'sweetalert2';
import './Login.css';

const Login = () => {
  
  const [email, setEmail] = useState('');
  const handleLogin = (e) => {
    e.preventDefault();
    if (email.trim() !== '' ) {
      Swal.fire({
        icon: 'success',
        title: 'Email Send!',
        showConfirmButton: false,
        timer: 1500,
      });
     
    } else {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Please enter valid email !',
      });
    }
  };

  return (
    <div className="login-container">
      <h1>STAY  </h1>
      <h2>INFORMED ON OUR  </h2>
      <h3>LATEST NEWS!</h3>
      <form onSubmit={handleLogin} className="login-form">
        <input className='email'
          type="email"
          id='email'
          placeholder="Email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          required
        />
        <button type="submit" className='botton'>Send</button>
      </form>
    </div>
  );
};

export default Login;

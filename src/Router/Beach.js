import React from 'react'
import Navbar from '../companenthome/Navbar'
import Footer from '../companenthome/Footer'
import Homeimg from '../CompnantBeach/Homeimg'
import Ifni from '../CompnantBeach/Ifni'
import Taghazout from '../CompnantBeach/Taghazout'
import Dakhla from '../CompnantBeach/Dakhla'
import Tarfaya from '../CompnantBeach/Tarfaya'

const Beach = () => {
  return (
    <div>
      <Navbar/>
      <Homeimg/>
      <Ifni/>
      <Taghazout/>
      <Dakhla/>
      <Tarfaya/>
      <Footer/>
    </div>
  )
}

export default Beach

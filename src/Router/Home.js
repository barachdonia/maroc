import React from 'react'
import Navbar from '../companenthome/Navbar'
import Homeimg from '../companenthome/Homeimg'
import Destination from '../companenthome/Destination'
import Tradition from '../companenthome/Tradition'
import Food from '../companenthome/Food'
import Artcult from '../companenthome/Artcult'
import Footer from '../companenthome/Footer'
import Search from '../companenthome/Search'
import Preload from '../companenthome/Preload'

const Home = () => {
  return (
    <div>
     <Navbar/>
     <Homeimg/>
     <Destination/>
     <Tradition/>
    <Food/>
    <Artcult/>
    <Footer/>
  </div>
  )
}

export default Home

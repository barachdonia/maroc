import React from 'react'
import Navbar from '../companenthome/Navbar'
import Footer from '../companenthome/Footer'
import Homebustling from '../CompanentBustling/Homebustling'
import Marakech from '../CompanentBustling/Marakech'
import Casablanca from '../CompanentBustling/Casablanca'
import Meknes from '../CompanentBustling/Meknes'
import Fes from '../CompanentBustling/Fes'
import Agadir from '../CompanentBustling/Agadir'
import Rabat from '../CompanentBustling/Rabat'

const Bustling = () => {
  return (
    <div>
     <Navbar/>
     <Homebustling/>
     <Marakech/>
     <Casablanca/>
     <Meknes/>
     <Fes/>
     <Agadir/>
     <Rabat/>
     <Footer/>
    </div>
  )
}

export default Bustling

import React from 'react'
import Navbar from '../companenthome/Navbar'
import Homeimg from '../CompenetDesert/homeimg'
import Marzouga from '../CompenetDesert/marzouga'
import Agafay from '../CompenetDesert/Agafay'
import Warzazat from '../CompenetDesert/Warzazat'
import Footer from '../companenthome/Footer'
import Laayoune from '../CompenetDesert/Laayoune'
const Desert = () => {
  return (
    <div>
     <Navbar/>
     <Homeimg/>
     <Laayoune/>
     <Marzouga/>
     <Agafay/>
     <Warzazat/>
     <Footer/>
    </div>
  )
}

export default Desert

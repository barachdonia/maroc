import React from 'react'
import Fistival1 from '../photomoro/Imilchil.jpg'
import Fistival2 from '../photomoro/gnawa.jpg'
const Fistival = () => {
  return (
    <div className='artculture'>
      <div className='art'>
        <div className='imagess'>
        <img src={Fistival1} alt='img'/>
        <img src={Fistival2} alt='img'/>

      </div>
        <div className='art-text'>
<h2>Festivals </h2>
 <p>Morocco hosts vibrant festivals throughout the year, 
    including Imilchil Marriage Festival , the Marrakech
     International Film Festival, and the Gnaoua World Music Festival. These events celebrate diverse cultural expressions, 
    from sacred music to cinema and traditional genres, showcasing Morocco's rich heritage.</p>
        </div>
      
      </div>
    </div>
  )
}

export default Fistival

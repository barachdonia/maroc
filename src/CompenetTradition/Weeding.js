import React from 'react'
import weeding from '../photomoro/weeding.jpg'
import weeding2 from '../photomoro/weeding2.jpg'
const Weeding = () => {
  return (
    <div className='all'>
       
      <div className='destination'>
        <div className='des-text'>
<h2>Moroccan weeding </h2>
 <p>Moroccan weddings are vibrant and diverse, marked
     by negotiations, the henna ceremony, and a lively Zaffa procession. 
     The bride wears an elaborate Kaftan, and the wedding day includes a
      mosque ceremony and a lavish feast. Live music, traditional dances,
       and gift exchanges add to the festive atmosphere. Moroccan weddings typically span three days,
     blending tradition with modern celebrations.</p>
        </div>
      <div className='image'>
        <img src={weeding} alt='img'/>
        <img src={weeding2} alt='img'/>
      </div>
      </div>
    </div>
  )
}

export default Weeding

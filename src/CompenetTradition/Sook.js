import React from 'react'
import sook1 from '../photomoro/sook.jpg'
import sook2 from '../photomoro/sook2.jpg'
const Sook = () => {
  return (
    <div className='tradcomp'>
      <div className='tradition'>
        <div className='images'>
        <img src={sook1} alt='img'/>
        <img src={sook2} alt='img'/>

      </div>
        <div className='trad-text'>
<h2>Sook </h2>
 <p>A Moroccan souk is a bustling market where you can find 
    a variety of goods, from textiles to spices and traditional crafts.
     These vibrant markets, known for narrow alleys and diverse offerings,
      are an integral part
     of Moroccan culture and popular destinations for locals and tourists alike.</p>
        </div>
      
      </div>
    </div>
  )
}

export default Sook

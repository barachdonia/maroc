import React from 'react'
import homeimg from'../photomoro/weeding.jpg'
import './Hometradtion.css'


const Traditionhome = () => {
  return (
    <div className='traditionhome'>
        <div className='masketradition'>
            <img src={homeimg} alt='imghomeart' className='imghometradition'/>
        </div>
        <div className='content-tradition'>
          <h2>Moroccan </h2>
          <h1> Tradition </h1>
        </div>
        <p>where ancient customs dance with progress. </p>
    </div>
  )
}

export default Traditionhome
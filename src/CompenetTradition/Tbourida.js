import React from 'react'
import tbourida1 from '../photomoro/tbourida.jpg'
import tbourida2 from '../photomoro/tbourida2.jpg'
const Tbourida = () => {
  return (
    <div>
      <div className='Food'>
      
      <div className='food'>
        <div className='food-text'>
<h2>Tbourida </h2>
 <p>Tbourida, or Fantasia, is a dynamic Moroccan equestrian performance
     featuring riders in traditional attire charging in formation with 
     rifles, creating a dramatic and culturally significant display. This thrilling 
    show is a highlight at festivals, celebrating Morocco's rich equestrian heritage. </p>
        </div>
      <div className='imag'>
        <img src={tbourida1} alt='img'/>
        <img src={tbourida2} alt='img'/>
      </div>
      </div>
    </div>
    </div>
  )
}

export default Tbourida

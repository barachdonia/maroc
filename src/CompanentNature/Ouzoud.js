import React from 'react'
import ouzoud from '../photomoro/ouzoud.jpg'
import ouzoud2 from '../photomoro/ouzoud2.jpg'
const Ouzoud = () => {
  return (
    <div className='tradcomp'>
    <div className='tradition'>
      <div className='images'>
      <img src={ouzoud} alt='img'/>
      <img src={ouzoud2} alt='img'/>

    </div>
      <div className='trad-text'>
<h2>Ouzoud waterfall </h2>
<p>Ouzoud Waterfalls, near Tanaghmeilt in Morocco, 
    are the highest in North Africa at about 110 meters. 
    Surrounded by lush greenery, 
    they create a stunning natural spectacle and a popular destination for visitors.</p>
      </div>
    
    </div>
  </div>
  )
}

export default Ouzoud

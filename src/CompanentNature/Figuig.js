import React from 'react'
import figuig from '../photomoro/Figuig.jpg'
import figuig2 from '../photomoro/Figuig2.jpg'
const Figuig = () => {
  return (
    <div className='tradcomp'>
    <div className='tradition'>
      <div className='images'>
      <img src={figuig} alt='img'/>
      <img src={figuig2} alt='img'/>

    </div>
      <div className='trad-text'>
<h2>Figuig</h2>
<p>Figuig, in southeastern Morocco, is an oasis town surrounded by the Sahara Desert. Known
     for its lush palm groves and oases, Figuig offers a contrast to the arid landscape.</p>
      </div>
    
    </div>
  </div>
  )
}

export default Figuig

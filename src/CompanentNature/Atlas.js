import React from 'react'
import marzouga1 from '../photomoro/atlas.jpg'
import marzouga2 from '../photomoro/atlas2.jpg'
const Atlas = () => {
  return (
    <div className='all'>
    <div className='destination'>
      <div className='des-text'>
<h2>Atlas Mountains  </h2>
<p>The Atlas Mountains in Morocco span across the country,
     with the High Atlas being the highest range. It includes peaks 
     like Mount Toubkal. The Middle Atlas is known for its forests,
      and the Anti-Atlas has rocky terrains. These mountains showcase diverse
       and scenic landscapes, 
    attracting visitors for trekking, nature, and cultural experiences..</p>
      </div>
    <div className='image'>
      <img src={marzouga1} alt='img'/>
      <img src={marzouga2} alt='img'/>
    </div>
    </div>
  </div>
  )
}

export default Atlas

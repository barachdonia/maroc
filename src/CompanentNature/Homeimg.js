import React from 'react'
import imgbg from '../photomoro/nature.jpg'
const Homeimg = () => {
  return (
    <div className='desthome'>
    <div className='maske'>
        <img className='img-home'  src={imgbg} alt='imgbg'/>
    </div>
    <div className='content-home'>
      <h2>The North of </h2>
      <h1> Morocco </h1>
    </div>
    <p>Morocco's north: where the soul finds its way home. </p>
</div>
  )
}

export default Homeimg

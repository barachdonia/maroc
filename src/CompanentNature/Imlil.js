import React from 'react'
import imlil from '../photomoro/imlil.jpg'
import imlil2 from '../photomoro/imlil2.jpg'
const Imlil = () => {
  return (
    <div>
    <div className='Food'>
    
    <div className='food'>
      <div className='food-text'>
<h2>Imlil </h2>
<p>Imlil, nestled in the High Atlas Mountains of Morocco,
     is a trekking hub and gateway to Mount Toubkal. Offering stunning mountain views
     and a glimpse into Berber culture, it's a popular destination for hiking enthusiasts. </p>
      </div>
    <div className='imag'>
      <img src={imlil} alt='img'/>
      <img src={imlil2} alt='img'/>
    </div>
    </div>
  </div>
  </div>
  )
}

export default Imlil

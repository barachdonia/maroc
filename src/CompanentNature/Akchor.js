import React from 'react'
import akchour from '../photomoro/akchour.jpg'
import akchour2 from '../photomoro/akchour2.jpg'
const Akchor = () => {
  return (
    <div>
    <div className='Food'>
    
    <div className='food'>
      <div className='food-text'>
<h2>Akchour </h2>
<p>Akchour, near Chefchaouen in Morocco, 
    features the stunning natural arch, the Bridge of God. 
    This picturesque site is set in the scenic Rif
     Mountains and is a popular destination for its natural beauty and hiking opportunities. </p>
      </div>
    <div className='imag'>
      <img src={akchour} alt='img'/>
      <img src={akchour2} alt='img'/>
    </div>
    </div>
  </div>
  </div>
  )
}

export default Akchor

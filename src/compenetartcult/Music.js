import React, { useState ,useRef ,createRef} from 'react';
import"./Art.css"
import Video3 from '../Vedios/malhoun2.mp4'
import Video2  from '../Vedios/gnawa2.mp4'
import Video1 from '../Vedios/ahidous3.mp4'
import Video4  from '../Vedios/marakechh.mp4'
import Video5  from '../Vedios/chaabi.mp4'
import Video6  from '../Vedios/Aita.mp4'
import Video7  from '../Vedios/Abidatrma.mp4'
import Video8  from '../Vedios/Sahrawi.mp4'
import 'bootstrap/dist/css/bootstrap.min.css';
const Music = () => {
  const videoSources = [
    {
      id: 1,
      title: "AHIDOUS",
      srcs: Video1
    },
    {
      id: 2,
      title: "GNAWA",
      srcs: Video2
    },
    {
      id: 3,
      title: "MALHOUN",
      srcs: Video3
    },
    {
      id: 4,
      title: "Marakech Daka",
      srcs: Video4
    },
    {
      id: 5,
      title: "CHAABI",
      srcs: Video5
    },{
      id: 6,
      title: "AITA",
      srcs: Video6
    },{
      id: 7,
      title: "ABIDAT RMA",
      srcs: Video7
    },{
      id: 8,
      title: "HASSANI",
      srcs: Video8
    },
    
  ];

  const videoRefs = useRef(videoSources.map(() => createRef()));
  const [isPlaying, setIsPlaying] = useState(Array(videoSources.length).fill(false));

  const handleTogglePlay = (index) => {
    const newIsPlaying = [...isPlaying];
    if (newIsPlaying[index]) {
      videoRefs.current[index].current.pause();
    } else {
      videoRefs.current[index].current.play();
    }
    newIsPlaying[index] = !newIsPlaying[index];
    setIsPlaying(newIsPlaying);
  };

  return (
    <>
     <h1 className='title'>Music</h1>
      {videoSources.map((videoSource, index) => (
        <div className='col' key={videoSource.id}>
          <h2>{videoSource.title}</h2>
          <div className='vedios'>
            <video ref={videoRefs.current[index]} width="400">
              <source src={videoSource.srcs} type="video/mp4" />
            </video>
          </div>
          <button className='btn-play' onClick={() => handleTogglePlay(index)}>
              {isPlaying[index] ? 'Pause' : 'Play'}
            </button>
        </div>
      ))}
    </>
  );
};

export default Music
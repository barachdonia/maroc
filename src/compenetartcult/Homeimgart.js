import React from 'react'
import homeimg from'../photomoro/artculture.jpg'
import './homeimgart.css'
const Arthome = () => {
  return (
    <div className='arthome'>
        <div className='maskeart'>
            <img src={homeimg} alt='imghomeart' className='imghomeart'/>
        </div>
        <div className='content-art'>
          <h1>The Moroccan    </h1>
          <h2> Culture&Art </h2>
        </div>
        <p>Discover Morocco's allure in every step you take. </p>
    </div>
  )
}

export default Arthome

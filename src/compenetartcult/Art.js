import React from 'react'
import"./Art.css"
import zlij from '../photomoro/zlij3.jpg'
import architecture from '../photomoro/architecture.jpg'
import tissage from '../photomoro/tissage.jpg'
import hana from '../photomoro/hanae.jpg'
import Woodwork from '../photomoro/Woodwork.jpg'
import Ceramics from '../photomoro/Ceramics.jpg'
import Metalwork from '../photomoro/Metalwork.jpg'
import painture from '../photomoro/painture.jpg'
const Art = () => {
  return (
    <div>
      <h1 className='title'>Art</h1>
          <div className="card1">
          <img src={Woodwork} alt='Art'/>
    <div className="card-content">
        <h3 className="card-title">Woodwork</h3>
        <p className="card-paragraph">Moroccan woodwork is characterized by
         its detailed carving and ornate designs. Wooden doors, windows, 
         and furniture often showcase geometric patterns and arabesques. 
        The craft of carving is passed down through generations.</p>
    </div>
    
</div>

<div class="card1">
    <img src={Ceramics} alt='Art'/>
    <div className="card-content">
        <h3 className="card-title">Pottery and Ceramics</h3>
        <p className="card-paragraph">Moroccan pottery is known for its vibrant
         colors and intricate designs. Safi and Fes are renowned for their pottery 
         traditions. Traditional ceramic items include plates, bowls, 
        and decorative tiles adorned with geometric patterns.</p>
    </div>
</div>
<div class="card1">
    <img src={tissage} alt='Art'/>
    <div className="card-content">
        <h3 className="card-title">Carpet and Textile Art</h3>
        <p className="card-paragraph"> Moroccan carpets and textiles
         are highly prized for their quality and unique designs. Berber carpets, 
         in particular, are known for their tribal patterns and use of natural dyes. 
        The patterns often have symbolic meanings related to Berber culture.</p>
    </div>
</div>
<div class="card1">
    <img src={architecture} alt='Art'/>
    <div className="card-content">
        <h3 className="card-title">Islamic Art and Architecture</h3>
        <p className="card-paragraph">Morocco has a rich tradition 
        of Islamic art and architecture, evident in its mosques, palaces, 
        and madrasas. Traditional geometric patterns, arabesques, and calligraphy
         are prominent features
         in Moroccan Islamic art.</p>
    </div>
</div> 
<div class="card1">
    <img src={zlij} alt='Art'/>
    <div className="card-content">
        <h3 className="card-title">Zellige (Mosaic Tilework)</h3>
        <p className="card-paragraph"> Zellige is a distinctive form of mosaic
         tilework  . It involves intricate geometric 
         patterns created by arranging colorful, glazed tiles into complex designs.
         Zellige is commonly used to decorate walls, floors, and fountains.</p>
    </div>
</div>
<div class="card1">
    <img src={Metalwork} alt='Art'/>
    <div className="card-content">
        <h3 className="card-title">Metalwork</h3>
        <p className="card-paragraph">Moroccan metalwork is a
         traditional craft that reflects the country's rich artistic heritage.
          Skilled artisans in Morocco have been creating intricate metalwork for centuries, 
        producing a wide range of decorative and functional items. </p>
    </div>
</div>
<div class="card1">
    <img src={hana} alt='Art'/>
    <div className="card-content">
        <h3 className="card-title">Henna Art</h3>
        <p className="card-paragraph">  Henna art, also known as Mehndi, 
        is a form of temporary body decoration that involves applying henna paste to
         create intricate and decorative designs on the skin. In Morocco,  
        and it is often associated with special occasions and celebrations.</p>
    </div>
</div>
<div class="card1">
    <img src={painture} alt='Art'/>
    <div className="card-content">
        <h3 className="card-title">painture Art</h3>
        <p className="card-paragraph">Moroccan painting: Blend of Islamic, Arab, Berber influences. Vibrant colors, geometric patterns, nature-inspired motifs. 
        Mediums: watercolor, gouache, ink. Common in ceramics, textiles, woodwork.</p>
    </div>
</div>
    </div>
  )
}

export default Art

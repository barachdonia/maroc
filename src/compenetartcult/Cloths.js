import React from 'react'
import"./Art.css"
import caftan from '../photomoro/cloths/caftan.jpg'
import imazigh from '../photomoro/cloths/imazigh.jpg'
import jabadour from '../photomoro/cloths/jabadourr.jpg'
import men from '../photomoro/cloths/men2.jpg'
import sahrawi from '../photomoro/cloths/sahrawi.jpg'
import sahrawi1 from '../photomoro/cloths/sahrawi1.jpg'
import tkchita from '../photomoro/cloths/tkchita2.jpg'
import men3 from '../photomoro/cloths/men3.jpg'
import hayk from '../photomoro/cloths/hayk.jpg'
import North from '../photomoro/cloths/cloths3.jpg'
const Cloths = () => {
  return (
    <div>
     <h1 className='title'>Cloths</h1>
<div class="card3">
    <img src={caftan} alt='cloths2'/>
    <div className="card-content">
        <h3 className="card-title">caftan</h3>
    </div>
</div>
<div class="card3">
    <img src={imazigh} alt='cloths2'/>
    <div className="card-content">
        <h3 className="card-title">Imazighn cloths(Berbar)</h3>
        <p className="card-paragraph"></p>
    </div>
</div>
<div class="card3">
    <img src={jabadour} alt='cloths2'/>
    <div className="card-content">
        <h3 className="card-title">Jabadour</h3>
        <p className="card-paragraph">  </p>
    </div>
</div>
<div class="card3">
    <img src={men} alt='cloths2'/>
    <div className="card-content">
        <h3 className="card-title">Men cloths</h3>
        <p className="card-paragraph"> </p>
    </div>
</div>
<div class="card3">
    <img src={sahrawi1} alt='cloths2'/>
    <div className="card-content">
        <h3 className="card-title">Sahara cloths (Men&Women)</h3>
        <p className="card-paragraph"> </p>
    </div>
    </div>
    <div class="card3">
    <img src={North} alt='cloths2'/>
    <div className="card-content">
        <h3 className="card-title">North cloths (Men&Women)</h3>
        <p className="card-paragraph"> </p>
    </div>
</div>
<div class="card3">
    <img src={tkchita} alt='cloths2'/>
    <div className="card-content">
        <h3 className="card-title">Takchita</h3>
        <p className="card-paragraph"> </p>
    </div>
</div>
<div class="card3">
    <img src={sahrawi} alt='cloths2'/>
    <div className="card-content">
        <h3 className="card-title">Sahara cloths (Women)</h3>
        <p className="card-paragraph"> </p>
    </div>
</div>
<div class="card3">
    <img src={men3} alt='cloths2'/>
    <div className="card-content">
        <h3 className="card-title">Men cloths (jabadour)</h3>
        <p className="card-paragraph"> </p>
    </div>
</div>
<div class="card3">
    <img src={hayk} alt='cloths2'/>
    <div className="card-content">
        <h3 className="card-title">old moroccan cloths(Women HAYK)</h3>
        <p className="card-paragraph"> </p>
    </div>
</div>
 
    </div>
  )
}

export default Cloths

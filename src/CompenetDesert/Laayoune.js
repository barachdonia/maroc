import React from 'react'
import laayoun from '../photomoro/laayoun.jpg'
import laayoun2 from '../photomoro/laayoun2.jpg'
const Laayoune = () => {
  return (
    <div className='tradcomp'>
      <div className='tradition'>
        <div className='images'>
        <img src={laayoun2} alt='img'/>
        <img src={laayoun} alt='img'/>
      </div>
        <div className='trad-text'>
<h2>Laayoune </h2>
 <p>Laayoune is the largest city in Moroccan Sahara,
     serving as its de facto capital. It's an administrative and economic center</p>
        </div>
      </div>
    </div>
  )
}

export default Laayoune

import React from 'react'
import marzouga1 from '../photomoro/marzoyga1.jpg'
import marzouga2 from '../photomoro/marzouga2.jpg'
const Marzouga = () => {
  return (
    <div className='all'>
      <div className='destination'>
        <div className='des-text'>
<h2>Marzouga </h2>
 <p>Merzouga is a Moroccan village in the Sahara Desert, known for 
    its captivating Erg Chebbi dunes, rising up to 150 meters. 
    Visitors experience the desert's beauty through camel treks, witness stunning sunrises and sunsets,
     and stay in desert camps offering a unique blend of adventure and Berber culture..</p>
        </div>
      <div className='image'>
        <img src={marzouga1} alt='img'/>
        <img src={marzouga2} alt='img'/>
      </div>
      </div>
    </div>
  )
}

export default Marzouga

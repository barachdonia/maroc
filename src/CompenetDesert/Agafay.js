import React from 'react'
import agafay2 from '../photomoro/Agafay.jpg'
import Agafay1 from '../photomoro/agafay2.jpg'
const Agafay = () => {
  return (
    <div className='tradcomp'>
      <div className='tradition'>
        <div className='images'>
        <img src={agafay2} alt='img'/>
        <img src={Agafay1} alt='img'/>

      </div>
        <div className='trad-text'>
<h2>Agafay </h2>
 <p>Agafay is a desert-like region in the High Atlas Mountains near Marrakech, known for its arid and rocky landscape. Visitors can enjoy activities like camel
     riding and quad biking, offering a unique desert experience close to the city.</p>
        </div>
      
      </div>
    </div>
  )
}

export default Agafay

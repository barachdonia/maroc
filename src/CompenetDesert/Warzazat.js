import React from 'react'
import warzazat from '../photomoro/warzazat.jpg'
import warzazat2 from '../photomoro/warzazat2.jpg'
const Warzazat = () => {
  return (
    <div>
    <div className='Food'>
    
    <div className='food'>
      <div className='food-text'>
<h2>Warzazat </h2>
<p>"Ouarzazate," it is a city located in the southeastern part of Morocco. 
    Ouarzazate is known for its stunning landscapes, including the nearby Atlas Mountains and the Draa Valley. The city is often referred to as the "Gateway to the Sahara" and is famous for its historic kasbahs (fortresses),
     film studios, and the UNESCO World Heritage site of Ait Benhaddou. </p>
      </div>
    <div className='imag'>
      <img src={warzazat} alt='img'/>
      <img src={warzazat2} alt='img'/>
    </div>
    </div>
  </div>
  </div>
  )
}

export default Warzazat

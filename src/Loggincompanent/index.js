import React, { useState } from 'react';
import './Style.css'; // Assuming you have a CSS file named 'style.css'
import {FaInstagram,FaFacebook,FaGoogle} from "react-icons/fa"
const Login = () => {
    const [active, setActive] = useState(false);

    const toggleForm = () => {
        setActive(!active);
    };

    return (
        <div className={`container ${active ? 'active' : ''}`} id="container">
            <div className="form-container sign-up">
                <form>
                    <h1>Create Account</h1>
                    <div className="social-icons">
                    <a href="#" className="icon">< FaFacebook /></a>
                        <a href="#" className="icon">< FaInstagram /></a>
                        <a href="#" className="icon">< FaGoogle /></a>
                    </div>
                    <span>or use your email for registration</span>
                    <input type="text" placeholder="Name" />
                    <input type="email" placeholder="Email" />
                    <input type="password" placeholder="Password" />
                    <button>Sign Up</button>
                </form>
            </div>
            <div className="form-container sign-in">
                <form>
                    <h1>Sign In</h1>
                    <div className="social-icons">
                    <a href="#" className="icon">< FaFacebook /></a>
                        <a href="#" className="icon">< FaInstagram /></a>
                        <a href="#" className="icon">< FaGoogle /></a>
                    </div>
                    <span>or use your email password</span>
                    <input type="email" placeholder="Email" />
                    <input type="password" placeholder="Password" />
                    <a href="#">Forget Your Password?</a>
                    <button>Sign In</button>
                </form>
            </div>
            <div className="toggle-container">
                <div className="toggle">
                    <div className="toggle-panel toggle-left">
                        <h1>Welcome Back!</h1>
                        <p>Enter your personal details to use all of site features</p>
                        <button className="hidden" onClick={toggleForm}>Sign In</button>
                    </div>
                    <div className="toggle-panel toggle-right">
                        <h1>Hello, Friend!</h1>
                        <p>Register with your personal details to use all of site features</p>
                        <button className="hidden" onClick={toggleForm}>Sign Up</button>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Login;

import React from 'react'
import ifni from '../photomoro/ifni.jpg'
import ifni2 from '../photomoro/ifni2.jpg'
const Ifni = () => {
  return (
    <div className='all'>
    <div className='destination'>
      <div className='des-text'>
<h2>Sidi ifni  </h2>
<p>Sidi Ifni, a coastal town in southwestern Morocco
    boasts a blend of Spanish-influenced Art Deco architecture. 
    Known for its scenic beachfront along the Atlantic, it offers a unique cultural atmosphere and a tranquil retreat for those seeking a quieter coastal experience. The town has historical 
    significance from Spanish colonization and is appreciated for its well-preserved charm..</p>
      </div>
    <div className='image'>
      <img src={ifni} alt='img'/>
      <img src={ifni2} alt='img'/>
    </div>
    </div>
  </div>
  )
}

export default Ifni

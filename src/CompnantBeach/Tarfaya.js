import React from 'react'
import tarfaya from '../photomoro/tarfaya.jpg'
import tarfaya2 from '../photomoro/tarfaya2.jpg'
const Tarfaya = () => {
  return (
    <div className='tradcomp'>
    <div className='tradition'>
      <div className='images'>
      <img src={tarfaya} alt='img'/>
      <img src={tarfaya2} alt='img'/>

    </div>
      <div className='trad-text'>
<h2>Tarfaya</h2>
<p>Tarfaya, a coastal town in 
    southwestern Morocco, holds historical 
    significance as the former station of Antoine 
    de Saint-Exupéry. With scenic beaches and proximity 
    to Cape Juby, it offers a mix of cultural heritage and coastal beauty.</p>
      </div>
    
    </div>
  </div>
  )
}

export default Tarfaya

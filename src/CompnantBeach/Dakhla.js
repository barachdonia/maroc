import React from 'react'
import dakhla from '../photomoro/dakhla.jpg'
import dakhla2 from '../photomoro/dakhla2.jpg'
const Dakhla = () => {
  return (
    <div>
    <div className='Food'>
    
    <div className='food'>
      <div className='food-text'>
<h2>Dakhla </h2>
<p>Dakhla is a coastal city known for its pristine white
     sand beaches and renowned as a top kitesurfing destination with consistent wind conditions. The area offers 
    various water sports and is home to a lagoon where pink flamingos can often be seen. </p>
      </div>
    <div className='imag'>
      <img src={dakhla} alt='img'/>
      <img src={dakhla2} alt='img'/>
    </div>
    </div>
  </div>
  </div>
  )
}

export default Dakhla

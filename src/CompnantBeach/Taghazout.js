import React from 'react'
import taghazout from '../photomoro/taghazout.jpg'
import taghazout2 from '../photomoro/tagh.jpg'
const Taghazout = () => {
  return (
    <div className='tradcomp'>
    <div className='tradition'>
      <div className='images'>
      <img src={taghazout} alt='img'/>
      <img src={taghazout2} alt='img'/>

    </div>
      <div className='trad-text'>
<h2>Taghazout </h2>
<p>Taghazout, a coastal village in Morocco, is a renowned surfing hub with world-class breaks. Known for its sandy beaches and relaxed atmosphere, 
    it attracts surfers and beach lovers seeking a tranquil escape.</p>
      </div>
    </div>
  </div>
  )
}

export default Taghazout

import React from 'react'
import meknes from '../photomoro/Meknes.jpg'
import Meknes2 from '../photomoro/Meknes2.jpg'
const Meknes = () => {
  return (
    <div>
    <div className='Food'>
    
    <div className='food'>
      <div className='food-text'>
<h2>Meknes </h2>
<p>Meknes, an imperial city in Morocco,
   is renowned for its well-preserved historical
    monuments like the Bab Mansour gate and Heri es-Souani
     granaries. With a vibrant medina and reflecting the legacy 
  of Moulay Ismail, it holds UNESCO recognition for its cultural significance. </p>
      </div>
    <div className='imag'>
      <img src={meknes} alt='img'/>
      <img src={Meknes2} alt='img'/>
    </div>
    </div>
  </div>
  </div>
  )
}

export default Meknes

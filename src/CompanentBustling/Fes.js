import React from 'react'
import Fes1 from '../photomoro/fes2.jpg'
import fes2 from '../photomoro/Fes1.jpg'
const Fes = () => {
  return (
    <div className='tradcomp'>
    <div className='tradition'>
      <div className='images'>
      <img src={fes2} alt='img'/>
      <img src={Fes1} alt='img'/>
    </div>
      <div className='trad-text'>
<h2>Fes</h2>
<p>Fes, a city in Morocco, is renowned for its historic 
  significance, with the Fes el-Bali medina, a UNESCO World
   Heritage Site. It's a hub of traditional craftsmanship, 
   including leather and ceramics, and hosts cultural events throughout the year. 
   is a living museum, blending history, culture, and spiritual significance.</p>
      </div>
    
    </div>
  </div>
  )
}

export default Fes

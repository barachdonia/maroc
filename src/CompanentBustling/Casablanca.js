import React from 'react'
import casa from '../photomoro/casa2.jpg'
import casa2 from '../photomoro/casablanca.jpg'
const Casablanca = () => {
  return (
    <div className='tradcomp'>
    <div className='tradition'>
      <div className='images'>
      <img src={casa} alt='img'/>
      <img src={casa2} alt='img'/>

    </div>
      <div className='trad-text'>
<h2>CasaBlanca </h2>
<p>Casablanca, Morocco's largest city,
   is a major economic hub with the largest
    port in North Africa. Known for its modern
   architecture, it's a bustling metropolis with a vibrant mix of business and culture.</p>
      </div>
    
    </div>
  </div>
  )
}

export default Casablanca

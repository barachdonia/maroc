import React from 'react'
import Marrakech1 from '../photomoro/jamfna.jpg'
import Marrakech2 from '../photomoro/jamfna2.jpg'
const Marakech = () => {
  return (
    <div className='all'>
    <div className='destination'>
      <div className='des-text'>
<h2>Marrakech  </h2>
<p>Marrakech, a vibrant city in Morocco,
   is famous for its UNESCO-listed medina,
    lively Jemaa el-Fnaa square, and historic palaces. 
  Rich in culture, it's a captivating blend of tradition and modernity.</p>
      </div>
    <div className='image'>
      <img src={Marrakech1} alt='img'/>
      <img src={Marrakech2} alt='img'/>
    </div>
    </div>
  </div>
  )
}

export default Marakech

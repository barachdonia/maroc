import React from 'react';
import { useParams } from 'react-router-dom';
import agadir from '../photomoro/agadir.jpg'
import agadir2 from '../photomoro/agadir2.jpg'
const Agadir = () => {
   
 
  return (
    <div >
    <div className='Food'>
    <div className='food'>
      <div className='food-text'>
<h2>Agadir </h2>
<p>Agadir, a coastal city in southern Morocco,
     is known for its long sandy beaches along 
     the Atlantic. The city, rebuilt after a 1960 earthquake, offers modern
     amenities and is a popular destination for beachgoers and water sports enthusiasts. </p>
      </div>
    <div className='imag'>
      <img src={agadir} alt='img'/>
      <img src={agadir2} alt='img'/>
    </div>
    </div>
  </div>
  </div>
  )
}

export default Agadir

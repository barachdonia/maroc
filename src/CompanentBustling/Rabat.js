import React from 'react'
import rabat1 from '../photomoro/rabat.jpg'
import rabat2 from '../photomoro/rabat2.jpg'
const Rabat = () => {
  return (
    <div className='tradcomp'>
    <div className='tradition'>
      <div className='images'>
      <img src={rabat2} alt='img'/>
      <img src={rabat1} alt='img'/>
    </div>
      <div className='trad-text'>
<h2>Rabat</h2>
<p>Rabat, Morocco's capital, merges history with contemporary life. Its iconic landmarks, 
    including the Kasbah of the Udayas and Hassan Tower, reflect its rich heritage. As a bustling administrative
     center and coastal city, Rabat offers a unique blend of tradition and modernity in North Africa.</p>
      </div>
    
    </div>
  </div>
  )
}

export default Rabat

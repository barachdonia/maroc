import React from 'react'
import imgbg from '../photomoro/jamaalfna.jpg'
const Homebustling = () => {
  return (
    <div className='desthome'>
        <div className='maske'>
            <img className='img-home'  src={imgbg} autoPlay loop muted/>
        </div>
        <div className='content-home'>
          <h2>Urban pulse of  </h2>
          <h1> Morocco </h1>
        </div>
        <p>In the city's hustle, dreams find their rhythm. </p>
    </div>
  )
}

export default Homebustling

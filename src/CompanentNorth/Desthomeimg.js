import React from 'react'
import North from '../photomoro/North.jpg'
import './Desthome.css'
const Desthome = () => {
  return (
    <div className='desthome'>
       <div className='maskeart'>
            <img src={North} alt='imghomeart' className='imghomeart'/>
        </div>
        <div className='content-home'>
          <h2>The North of  </h2>
          <h1> Morocco </h1>
        </div>
        <p>A blend of history, culture, and beauty that captivates the soul. </p>
    </div>
  )
}

export default Desthome

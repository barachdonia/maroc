import React from 'react';
import './Places.css'
import chefchaoun from "../photomoro/chefchaoun.jpg"
import chefchaoun2 from "../photomoro/chefchaoun2.jpg"
import tanger1 from "../photomoro/tanger1.jpg"
import tanger2 from "../photomoro/tanger2.jpg"
import Tetouan from "../photomoro/Tetouan.jpg"
import Tetouan1 from "../photomoro/Tetouan1.jpg"

const Places = () => {
  return (
    <div > 
      <div className='chefchaoun'>
        <div className='chefchaoun-text'>
<h2>Chefchaoun </h2>
 <p>
Chefchaouen, also known as the "Blue City," is a picturesque town in northern Morocco. 
Renowned for its striking blue-washed buildings and narrow,
 labyrinthine streets, Chefchaouen is nestled within the Rif Mountains.  </p>
        </div>
       
      <div className='imag-chefchaoun'>
        <img src={chefchaoun} alt='img'/>
        <img src={chefchaoun2} alt='img'/>
       
      </div>
      </div>
      <div className='Tanger'>
      <div className='image-Tanger'>
        <img src={tanger1} alt='img'/>
        <img src={tanger2} alt='img'/>
      </div>
      <div className='Tanger-text'>
<h2>Tanger </h2>
 <p>Tangier (also spelled Tangiers) is a fascinating port city in northern 
  Morocco known for its cultural diversity, historical significance, 
  and strategic location at the entrance to the Strait of Gibraltar. </p>
        </div>
      </div>
      <div className='Tetouan'>
        <div className='Tetouan-text'>
<h2>Tetouan </h2>
 <p>
 Tetouan is a city in northern Morocco with a rich history, 
 diverse culture, and significant architectural heritage.   </p>
        </div>
      <div className='imag-Tetouan'>
        <img src={Tetouan} alt='img'/>
        <img src={Tetouan1} alt='img'/>
       
      </div>
      </div>
      <div className='Ifran'>
      <div className='image-Ifran'>
        <img src={tanger1} alt='img'/>
        <img src={tanger2} alt='img'/>
      </div>
      <div className='Ifran-text'>
<h2>Ifran </h2>
 <p>Ifrane is a town located in the Middle Atlas region of 
  Morocco. Known for its distinctive architecture, European-style buildings, 
  and alpine-like ambiance, Ifrane is often referred to as the "Switzerland of Morocco"
   due to its resemblance to Swiss mountain resorts.</p>
        </div>
      </div>
    </div>
  );
};

export default Places;

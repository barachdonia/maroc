import React from 'react'
import homeimg from '../photomoro/Food/tajin4.jpg'
import"./Homefood.css"
import couscous1 from "../photomoro/couscous.jpg"
import couscous2 from "../photomoro/Food/couscous1.jpg"
import tajin1 from "../photomoro/Food/tajinkhodar.jpg"
import tajin2 from "../photomoro/Tajin2.jpg"
import rfissa1 from "../photomoro/Food/rfissa.jpg"
import rfissa2 from "../photomoro/Food/rfissa1.jpg"
import djaj1 from "../photomoro/Food/djaj.jpg"
import djaj2 from "../photomoro/Food/djaj1.jpg"
import harira1 from "../photomoro/Food/harira2.jpg"
import harira2 from "../photomoro/Food/hrira1.jpg"
import tanjiya1 from "../photomoro/Food/tanjiya.jpg"
import Tanjiya2 from "../photomoro/Food/tanjiya1.jpg"
import bstila1 from "../photomoro/Food/bstiladjaj.jpg"
import bstila2 from "../photomoro/Food/bstilla.jpg"
import msamen1 from "../photomoro/Food/msamen1.jpg"
import barir1 from "../photomoro/Food/barir1.jpg"
import tea1 from "../photomoro/Food/tea1.jpg"
import tea2 from "../photomoro/Food/tea2.jpg"
const Homefood = () => {
  return (
    <div className='home'>
    <div className='homefood'>
        <img className='img-homefood'  src={homeimg} alt='introimg'/>
    </div>
    <div className='foodhome'>
      <div className='content-food'>
         <h2> Moroccan </h2>
      <h1> Taste </h1>
      </div>
      <p>Moroccan cuisine: Centuries and cultures in every bite, a culinary masterpiece.</p>
    </div>
    <div className='Dishs'>
      <h1>Moroccan dishs</h1>
      <h3>Main dishes</h3>
      <div className='couscous'>
      <div className='image-couscous'>
        <img src={couscous1} alt='img'/>
        <img src={couscous2} alt='img'/>
      </div>
      <div className='couscous-text'>
          <h2>couscous </h2>
            <h4>Moroccan 7 Vegetables Couscous is a vibrant dish, showcasing the rich flavors of Morocco. 
              With a couscous base and a medley of carrots, zucchini, turnips, bell peppers, tomatoes, onions,
               and chickpeas, it's a visually appealing and nutritionally rich delight. Seasoned with aromatic spices like cumin, coriander, and cinnamon,
               it captures the essence of Moroccan culinary traditions, emphasizing fresh, local ingredients. </h4>
      </div>
      </div>
   
    
    <div className='tajin'>
    <div className='tajin-text'>
          <h2>Tajin </h2>
            <h4>Moroccan Tajine, a cornerstone of North African cuisine, 
              is a savory and aromatic dish named after the clay pot in which it is cooked. This slow-cooked stew features a diverse combination of ingredients, such as tender meats (lamb, chicken, or beef), vegetables, dried fruits, and a blend of exotic spices like cumin, coriander, saffron, and cinnamon. The result is a rich, flavorful,
               and fragrant masterpiece, often served with crusty bread.  </h4>
      </div>
      <div className='imag-tajin'>
        <img src={tajin1} alt='img'/>
        <img src={tajin2} alt='img'/>
       
      </div>
      </div>
      <div className='couscous'>
      <div className='image-couscous'>
        <img src={rfissa1} alt='img'/>
        <img src={rfissa2} alt='img'/>
      </div>
      <div className='couscous-text'>
          <h2>Rfissa </h2>
            <h4>"Moroccan Rfissa, a culturally significant dish, features shredded msemen 
              or torn traditional bread soaked in a flavorful broth with lentils, fenugreek,
               and aromatic spices. Slow-cooked with tender chicken, this comforting meal is garnished with fried onions and served alongside fenugreek-infused broth. Enjoyed during special occasions, Rfissa is a beloved
               part of Moroccan culinary heritage, boasting rich flavors and a unique blend of ingredients." </h4>
      </div>
      </div>
      <div className='tajin'>
    <div className='tajin-text'>
          <h2>Djaj Mhamer </h2>
            <h4>Djaj Mhamer is a Moroccan culinary delight featuring tender chicken slow-cooked in a fragrant blend of spices, including cumin, ginger, saffron, and cinnamon. This savory dish boasts a rich broth and is often served with crusty bread,  and in the celebritions 
              .making it a flavorful and comforting centerpiece in Moroccan cuisine.  </h4>
      </div>
      <div className='imag-tajin'>
        <img src={djaj1} alt='img'/>
        <img src={djaj2} alt='img'/>
       
      </div>
      </div>
      <div className='couscous'>
      <div className='image-couscous'>
        <img src={harira1} alt='img'/>
        <img src={harira2} alt='img'/>
      </div>
      <div className='couscous-text'>
          <h2>Harira(Soup) </h2>
            <h4>Harira is a Moroccan soup known for its rich
               flavors and hearty ingredients. Made with tomatoes,
                lentils, chickpeas, and a blend of aromatic spices,
                 including cumin and cinnamon, this nutritious soup often
                  features vegetables and meat. Harira is a popular dish 
                  during Ramadan and a beloved comfort food in Morocco,
                   showcasing the country's culinary heritage with 
              its wholesome and flavorful composition. </h4>
      </div>
      </div>
      <div className='tajin'>
    <div className='tajin-text'>
          <h2>Tanjiya </h2>
            <h4>Tanjiya is a traditional Moroccan dish, originating 
              from Marrakech, known for its slow-cooked, savory stew.
               Typically made with lamb, the dish is named after the 
               earthenware pot, also called "tanjiya," in which it is
                cooked. Ingredients include meat, garlic, preserved lemon,
                 and various spices, resulting in a fragrant and tender stew. Tanjiya is often slow-cooked in communal ovens, making it a flavorful and culturally significant dish enjoyed during
               social gatherings in Morocco.  </h4>
      </div>
      <div className='imag-tajin'>
        <img src={tanjiya1} alt='img'/>
        <img src={Tanjiya2} alt='img'/>
      </div>
      </div>
      <div className='couscous'>
      <div className='image-couscous'>
        <img src={bstila1} alt='img'/>
        <img src={bstila2} alt='img'/>
      </div>
      <div className='couscous-text'>
          <h2>Bastilla </h2>
            <h4>Bastilla is a Moroccan delicacy, 
              a savory and sweet pastry filled with spiced meat, often pigeon 
              or chicken, along with almonds and powdered sugar.
               The layers of flaky pastry create a delightful contrast of textures, 
              making Bastilla a unique and celebrated dish in Moroccan cuisine. </h4>
      </div>
      </div>
      <h3>Msamen/Bghrir</h3>
      <div className='tajin'>
    <div className='tajin-text'>
           
            <h4>Moroccan msamen is a
               flaky flatbread crafted with layers of butter, creating a rich and satisfying treat. Made with a 
               simple dough of flour, semolina, yeast, water, and salt, it's often served with honey, jam, 
               or cream cheese for a delightful snack or breakfast option.</h4>
<h4>Baghrir, or Moroccan "Thousand-Hole Pancakes,"
 are unique with their spongy texture and numerous tiny holes. The batter, composed of semolina, flour, yeast, sugar, and water, ferments to create the distinct appearance. Served with honey or syrup,
 baghrir is a delicious and visually appealing part of Moroccan cuisine.
  </h4>  
      </div>
      <div className='imag-tajin'>
        <img src={barir1} alt='img'/>
        <img src={msamen1} alt='img'/>
      </div>
      </div>
      <h3>Drinks</h3>
      <div className='tajin'>
    <div className='tajin-text'>
          <h2>Moroccan Tea </h2>
            <h4>Moroccan tea, or Moroccan mint tea, is a traditional green 
              tea infused with fresh mint leaves and sweetened with sugar. 
              Known for its strong flavor, 
              it's a symbol of hospitality and is often served during social occasions in Morocco. </h4>
      </div>
      <div className='imag-tajin'>
        <img src={tea1} alt='img'/>
        <img src={tea2} alt='img'/>
       
      </div>
      </div>
      </div>
   
    </div>
  )
}

export default Homefood

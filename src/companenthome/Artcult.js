import React from 'react'
import art from '../photomoro/art.jpg'
import camanja from '../photomoro/camanja.jpg'
import './artcultcss.css'


const Artcult = () => {
  return (
    <div className='artculture'>
      <div className='art'>
        <div className='imagess'>
        <img src={art} alt='img'/>
        <img src={camanja} alt='img'/>

      </div>
        <div className='art-text'>
<h2>Art&Culture </h2>
 <p>Morocco's art and culture burst with the colors of its diverse heritage. Arab, Berber, 
  and African influences weave together, 
  creating a unique tapestry of vibrant music, exquisite art, and delicious cuisine.</p>
        </div>
      
      </div>
    </div>
  )
}

export default Artcult

import React from 'react'
import chefchaoun from '../photomoro/chefchaoun.jpg'
import essaouira from '../photomoro/essaouira.jpg'
import './Destcss.css'


const Destination = () => {
  return (
    <div className='all'>
      <h1>Why Morocco</h1>
      <div className='destination'>
        <div className='des-text'>
<h2>Top destination  </h2>
 <p>Morocco has a tapestry of diverse landscapes, 
  each with its own unique appeal, 
  but when it comes to a combination of cultural richness and natural beauty, 
  you must visit Morocco.</p>
        </div>
      <div className='image'>
        <img src={chefchaoun} alt='img'/>
        <img src={essaouira} alt='img'/>

      </div>
      </div>
    </div>
  )
}

export default Destination

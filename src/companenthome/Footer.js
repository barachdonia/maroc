import React from 'react';
import './Footer.css'; 
import Login from '../Router/login.js'
import logo from "../photomoro/morocco tresor2.png"
import { FaCopyright } from 'react-icons/fa';
 

const Footer = () => {
  const pages = [
    { name: 'North', path:"/North"},
    { name: 'Desert', path: '/Desert' },
    { name: 'Nature', path: '/Nature' },
    { name: 'Beach', path: '/Beach' },
    { name: 'Bustling', path: '/Bustling' },
  ];
  const handleOptionChange = (event) => {
    const selectedPage = pages.find((page) => page.name === event.target.value);
    if (selectedPage) {
      window.location.href = selectedPage.path;
    }
  };

  return (
    <footer className="footer">
      <div className='footer-content'>
        <div className='footer-section about'>
          <img src={logo} alt='logo'/>
          <p>Morocco:Timeless charm.Bustling markets.Golden dunes.Exotic flavors.Welcoming smiles.Your adventure awaits. </p>
        </div>
        <div className='footer-section '>
          <div className='links'>
               <h1>Quicks Links</h1>
        <ul >
        <li>
            <a href='/'>Home</a>
        </li>
        <li>
        <select onChange={handleOptionChange}>
        <option value=""  hidden>Top destination</option>
        {pages.map((page, index) => (
          <option key={index} value={page.name}>
            {page.name}
          </option>
        ))}
      </select>
        </li>
        <li>
            <a href="/Art&Culture">Art&Culture</a>
        </li>
        <li>
            <a href="/Food">Food</a>
        </li>
        <li>
            <a href="/Tradition">Tradition</a>
        </li>
      </ul>
          </div>
       
        </div>
        <div className='footer-section '>
          <div className='subscribe'>
            <h1>Subscribe</h1>
          <Login/>
          </div>
          
        </div>
      </div>
      <div className='footer-bottom'>
      <p><FaCopyright size={10} style={{color:"rgb(4, 118, 65)"}}/>  MoroccoTresor|By Donia Barbach & Asaad Sbihi</p> 
      </div>
     
    </footer>
  );
};

export default Footer;

import React from 'react'
import tajin from '../photomoro/Tajin2.jpg'
import couscous from '../photomoro/couscous.jpg'
import './foddcss.css'


const Food = () => {
  return (
    <div className='Food'>
      
      <div className='food'>
        <div className='food-text'>
<h2>Food </h2>
 <p>Moroccan cuisine is a delightful fusion of flavors, spices, 
    and cultural influences that reflect the country's 
    diverse history and geography. </p>
        </div>
      <div className='imag'>
        <img src={tajin} alt='img'/>
        <img src={couscous} alt='img'/>
      </div>
      </div>
    </div>
  )
}

export default Food

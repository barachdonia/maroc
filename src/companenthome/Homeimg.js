import React from 'react'
import './homeimg.css'
import homeimg from '../photomoro/morocco.jpg'
import Search from './Search'

const Homeimg = () => {
  return (
    <div className='home'>
        <div className='mask'>
            <img className='into-img'  src={homeimg} alt='introimg'/>
        </div>
        <div className='content'>
          <h2>EXPLORE   </h2>
          <h1> Morocco </h1>
          <p >Discover Morocco's allure in every step you take. </p>
          <Search/>
        </div>
        
        
        
    </div>
  )
}

export default Homeimg

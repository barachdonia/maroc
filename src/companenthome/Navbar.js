import { Link } from "react-router-dom";
import './Navbar.css'
import React, { useState ,useEffect} from 'react'
import {FaBars,FaTimes,FaUser} from "react-icons/fa"
import logo from '../photomoro/morocco tresor2.png'
const Navbar = () => {
  const pages = [
    { name: 'North', path:"/North"},
    { name: 'Desert', path: '/Desert' },
    { name: 'Nature', path: '/Nature' },
    { name: 'Beach', path: '/Beach' },
    { name: 'Bustling', path:'/Bustling' },
  ];
  const handleOptionChange = (event) => {
    const selectedPage = pages.find((page) => page.name === event.target.value);
    if (selectedPage) {
      window.location.href = selectedPage.path;
    }
    
  };
  const [scrolling, setScrolling] = useState(false);

  useEffect(() => {
    const handleScroll = () => {
      if (window.scrollY > 50) {
        setScrolling(true);
      } else {
        setScrolling(false);
      }
    };
    window.addEventListener('scroll', handleScroll);
    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, []);
const [menu,setmenu]=useState(false);
const handlmenu=()=>setmenu(!menu);

  return (
    <div className={scrolling ? 'header scrolled' : 'header'}>
      <Link to="/" >
      <img className='logo'  src={logo} alt='logo'/>
      </Link>
      <ul className={menu ? "nav-menu active" : "nav-menu"}>
        <li>
            <Link to="/">Home</Link>
        </li>
        <li>
        <select onChange={handleOptionChange}>
        <option value=""  hidden>Top destination</option>
        {pages.map((page, index) => (
          <option key={index} value={page.name}>
            {page.name}
          </option>
        ))}
      </select>
        </li>
        <li>
            <Link to="/Art&Culture">Art&Culture</Link>
        </li>
        <li>
            <Link to="/Food">Food</Link>
        </li>
        
        <li>
            <Link to="/Tradition">Tradition</Link>
        </li>
        <li>
            <Link to="/login">< FaUser/></Link>
        </li>
      </ul>
      <div className="humberger" onClick={handlmenu}>
        {menu ? (< FaTimes size={20} style={{color:"red"}}/>): (  <FaBars  size={20} style={{color:"red"}} />)}
      </div>
    </div>
  )
}

export default Navbar

import React, { useEffect, useRef, useState } from 'react';
import axios from 'axios';
import './Search.css';

const Search = () => {
  const [data, setData] = useState([]);
  const [filteredData, setFilteredData] = useState([]);
  const inputRef = useRef();

  useEffect(() => {
    axios.get("http://localhost:3004/Items")
      .then(res => {
        setData(res.data);
      })
      .catch(err => console.error(err));
  }, []);

  const handleSearch = () => {
    const inputValue = inputRef.current.value.toLowerCase();
    if (inputValue.trim() === "") {
      setFilteredData([]);
    } else {
      const filtered = data.filter(item => 
        (item.item && item.item.toLowerCase().includes(inputValue))
      );
      setFilteredData(filtered);
    }
  };
 
  

  return (
    <div>
      <div className='Search'>
        <input
          type="text"
          placeholder="Inspired me ....."
          ref={inputRef}
          onChange={handleSearch}
           
        />
        <div className="result-container">
          <ul className="result-list">
          {filteredData.map((d, index) => (
            <li key={index}>
              <div>
                <h3>{d.item}</h3>
                 
                <a href={d.link}>See more about {d.item}</a>
              </div>
            </li>
          ))}
        </ul>
        </div>
      </div>
    </div>
  );
}

export default Search;

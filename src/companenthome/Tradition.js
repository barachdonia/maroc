import React from 'react'
import weeding from '../photomoro/mariage1.jpg'
import tbourida from '../photomoro/tbourida.jpg'
import './Traditioncss.css'


const Tradition = () => {
  return (
    <div className='tradcomp'>
      <div className='tradition'>
        <div className='images'>
        <img src={weeding} alt='img'/>
        <img src={tbourida} alt='img'/>
      </div>
        <div className='trad-text'>
<h2>Tradition </h2>
 <p>Morocco's diverse heritage shines through its traditions,
   reflecting Arab, Berber, African, and Mediterranean influences. From daily life to celebrations,
   experience the vibrant tapestry of this unique culture.</p>
        </div>
      
      </div>
    </div>
  )
}

export default Tradition

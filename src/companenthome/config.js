const express = require('express');
const nodemailer = require('nodemailer');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());
// Route to send email
app.post('/send-email', async (req, res) => {
  const { toEmail } = req.body;

  try {
    // Create a Nodemailer transporter
    let transporter = nodemailer.createTransport({
      service: 'Gmail',
      auth: {
        user: 'moroccotresor@gmail.com', // Your email address
        pass: 'moroccotresor1122' // Your password
      }
    });

    // Send mail with defined transport object
    let info = await transporter.sendMail({
      from: 'moroccotresor@gmail.com', // Your email address
      to: toEmail,
      subject: 'Hello ✔',
      text: 'We are excited to share updates, news, and special offers with you! Thank you for subscribing.',
    });

    console.log('Email sent: ' + info.response);
    res.status(200).send('Email sent successfully');
  } catch (error) {
    console.error('Error sending email:', error);
    res.status(500).send('Error sending email');
  }
});

// Start server
const port = process.env.PORT || 3001;
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});

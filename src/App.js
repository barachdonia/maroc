import React, { useEffect, useState } from "react";
import "./index.css"
import { Route, Routes} from "react-router-dom";
import Home from "./Router/Home";
import Aart from "./Router/Art&culture";
import Food from "./Router/Food";
import Tradition from "./Router/Tradition";
import North from "./Router/North";
import Desert from "./Router/Desert";
import Nature from "./Router/Nature";
import Beach from "./Router/Beach";
import Bustling from "./Router/Bustling";
import EmailSender from "./companenthome/Search";
import Preload from "./companenthome/Preload";
import Login from "./Loggincompanent";
 

function App() {

  const [isloading,setisloading]=useState(true);
  useEffect(()=>{
    const fakedatafetch=()=>{
      setTimeout(()=>{
setisloading(false);
      },800);
    }
    fakedatafetch();
  },[])
  return isloading ? (
    <Preload/>
    ):(
    <div>
      <Routes>
        <Route path="/" element={<Home/>}/>
        <Route path="/North" element={<North/>}/>
        <Route path="/Nature" element={<Nature/>}/>
        <Route path="/Beach" element={<Beach/>}/>
        <Route path="/Desert" element={<Desert/>}/>
        <Route path="/Bustling" element={<Bustling/>}/>
        <Route path="/Art&Culture" element={<Aart/>}/>
        <Route path="/Food" element={<Food/>}/>
        <Route path="/Tradition" element={<Tradition/>}/>
        <Route path="/search" element={<EmailSender/>}/>
        <Route path="/Login" element={<Login/>}/>
        
      </Routes>
    </div>
  
  );
}

export default App;
